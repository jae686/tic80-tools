import bpy

# TODO : Adicionar opção para aplicar transformações ao modelo
# Evitar dar cabo do modelo -> transformações apenas devem ser aplicadas ao modelo que deve ser escrito


def write_some_data(context, filepath, bApplyTranforms):
    output=open(filepath,"w")
    vertice_list = [] #lista de vertices
    vertices_indices = []
    vertice_list_len = 0

    if bApplyTranforms :
        obj = bpy.context.object
        matrix = obj.matrix_world.copy()
        for vert in obj.data.vertices:
            vert.co = matrix @ vert.co
        obj.matrix_world.identity()

    mesh = context.active_object.data
    for i,fc in enumerate(mesh.polygons):
        if len(fc.vertices) == 3:
        #mFaces.push([fc.vertices[0],fc.vertices[1],fc.vertices[2],-1])
            vertices_indices.append(fc.vertices[0])
            vertices_indices.append(fc.vertices[1])
            vertices_indices.append(fc.vertices[2])

    for v in mesh.vertices:
        vertice_list.append(v)

    vertice_indices_len = len(vertices_indices)



    output.write(" -- nr vertices %d \n" %vertice_indices_len)

    #for i in range(0,vertice_indices_len,3):
    #    output.write("{ %d , %d, %d } \n" %(vertices_indices[i], vertices_indices[i+1], vertices_indices[i+2]))
    for i in range(0,vertice_indices_len,3):
       # output.write("{ %d , %d, %d }" %(vertices_indices[i], vertices_indices[i+1], vertices_indices[i+2]))
        a = vertices_indices[i]
        b = vertices_indices[i+1]
        c = vertices_indices[i+2]
        output.write("table.insert(mesh,{{%.4f,%.4f,%.4f},{%.4f,%.4f,%.4f},{%.4f,%.4f,%.4f}})\n" % (vertice_list[a].co[0], vertice_list[a].co[1],vertice_list[a].co[2],vertice_list[b].co[0],vertice_list[b].co[1],vertice_list[b].co[2],vertice_list[c].co[0],vertice_list[c].co[1],vertice_list[c].co[2]))
   

    return {'FINISHED'}


# ExportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, EnumProperty
from bpy.types import Operator


class ExportSomeData(Operator, ExportHelper):
    """Export to VI TIC80 3D functions"""
    bl_idname = "export_test.some_data"  # important since its how bpy.ops.import_test.some_data is constructed
    bl_label = "Export Some Data"

    # ExportHelper mixin class uses this
    filename_ext = ".lua"

    filter_glob: StringProperty(
        default="*.lua",
        options={'HIDDEN'},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    # List of operator properties, the attributes will be assigned
    # to the class instance from the operator settings before calling.
    use_setting: BoolProperty(
        name="Aplicar Transformações",
        description="Aplicar tranformações ao modelo antes de exportar",
        default=True,
    )

    type: EnumProperty(
        name="Example Enum",
        description="Choose between two items",
        items=(
            ('OPT_A', "First Option", "Description one"),
            ('OPT_B', "Second Option", "Description two"),
        ),
        default='OPT_A',
    )

    def execute(self, context):
        return write_some_data(context, self.filepath, self.use_setting)


# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
    self.layout.operator(ExportSomeData.bl_idname, text="VI TIC80 3D mesh")


def register():
    bpy.utils.register_class(ExportSomeData)
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)


def unregister():
    bpy.utils.unregister_class(ExportSomeData)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)


if __name__ == "__main__":
    register()

    # test call
    bpy.ops.export_test.some_data('INVOKE_DEFAULT')
